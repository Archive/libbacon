
//gcc -g -Wall -o testmsg bacon-message-connection.c testmsg.c `pkg-config --libs --cflags glib-2.0`

#include <unistd.h>
#include <stdlib.h>
#include "bacon-message-connection.h"

static guint timeout_id;

static gboolean
exit_server (gpointer user_data)
{
	BaconMessageConnection *conn = (BaconMessageConnection *) user_data;

	bacon_message_connection_free (conn);
	exit (0);

	return FALSE;
}

static void
print_stuff (const char *msg, gpointer user_data)
{
	BaconMessageConnection *conn = (BaconMessageConnection *) user_data;

	if (timeout_id > 0) {
		g_source_remove (timeout_id);
		timeout_id = 0;
	}
	g_print ("msg: %s\n", msg);
	timeout_id = g_timeout_add (5 * 1000, (GSourceFunc) exit_server, conn);
}

int
main (int argc, char **argv)
{
	BaconMessageConnection *conn;

	conn = bacon_message_connection_new ("totem");
	g_log_set_always_fatal (G_LOG_LEVEL_CRITICAL);

	if (conn == NULL)
	{
		g_message ("Couldn't connect");
		return 1;
	}

	if (bacon_message_connection_get_is_server (conn) == TRUE)
	{
		GMainLoop *loop;
		g_message ("I'm a server");

		bacon_message_connection_set_callback (conn, print_stuff, conn);
		loop = g_main_loop_new (NULL, FALSE);
		g_main_loop_run (loop);
	} else {
		g_message ("I'm a client");
		bacon_message_connection_send (conn, argv[1] ? argv[1] : "Foobar\0");
		bacon_message_connection_free (conn);
	}

	return 0;
}

