
//gcc -g -Wall -o testmsg totem-message-connection.c main.c `pkg-config --libs --cflags glib-2.0`

#include <stdlib.h>

#include "bacon-message-connection.h"

static void
cb_client (const char *msg, gpointer user_data)
{
	BaconMessageConnection *conn = user_data;

	g_message ("Received message from server: %s", msg);
	//bacon_message_connection_send (conn, "ping");
	bacon_message_connection_free (conn);
	exit (0);
}

static void
cb_server (const gchar *msg, gpointer user_data)
{
	BaconMessageConnection *conn = user_data;

	g_message ("Received message from client: %s", msg);
	g_usleep (100000);
	bacon_message_connection_send (conn, "pong");
	g_usleep (1000000);
	bacon_message_connection_free (conn);
	exit (0);
}

static void
setup (const char * cmd)
{
	BaconMessageConnection *conn;

	conn = bacon_message_connection_new ("totem");
	if (conn == NULL) {
		g_message ("Couldn't connect");
		exit (1);
	}
	if (bacon_message_connection_get_is_server (conn) == TRUE) {
		g_message ("I'm server");
		bacon_message_connection_set_callback (conn, cb_server, conn);
		g_spawn_command_line_async (cmd, NULL);
		g_message ("Spawned client");
	} else {
		g_message ("I'm client - ping");
		bacon_message_connection_set_callback (conn, cb_client, conn);
		bacon_message_connection_send (conn, "ping");
	}
}

int
main (int argc, char **argv)
{
	GMainLoop *loop;

	setup (argv[0]);

	loop = g_main_loop_new (NULL, FALSE);
	g_main_loop_run (loop);

	return 0;
}

